FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8002
COPY target/ufs-discovery-server-0.0.1-SNAPSHOT.jar app.jar
#COPY config/application.properties config/application.properties

ENV TZ=Africa/Nairobi
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


ENTRYPOINT java -Djava.security.egd=file:/dev/./urandom -jar /app.jar
