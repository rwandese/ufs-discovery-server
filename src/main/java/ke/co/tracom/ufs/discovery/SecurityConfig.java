/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.tracom.ufs.discovery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Configured security context with in memory users
 *
 * @author cornelius
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        log.info("Password Encoded: " + this.passwordEncoder.encode("Data2018."));
        http
                .authorizeRequests()
                //          .antMatchers("/login*").anonymous()
                .anyRequest().authenticated()
                .and()
                .httpBasic()
                .and()
                .csrf().disable() //          .formLogin()
                //          .loginPage("/login.html")
                //          .defaultSuccessUrl("/homepage.html")
                //          .failureUrl("/login.html?error=true")
                //          .and()
                //          .logout().logoutSuccessUrl("/login.html")
                ;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
